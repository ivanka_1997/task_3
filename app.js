var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var usersList = require('./routes/usersList');
// var userID = require('./routes/userID');
var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/user', usersList);
app.use('/', indexRouter);
app.use('/users', usersRouter);

// app.use('/user/:id', userID);
module.exports = app;